
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: JMOmandown 

** IMPORTANT **
Ensure the latest development (dev) version of Charts is installed or functions
used to render results in graphical format will not be available

Active Campaign is a Module used to connect and communicate with the 
Active Campaign service through their provided offsite api.  It allows
for Drupal to connect, manage, add, edit, delete, and review results from
services provided by Active Campaign.

Active Campaign provides email market services.


INSTALLATION
------------

The module can be installed and enabled by the following steps.

1. Copy the active_campaign directory to your sites/SITENAME/modules directory.

2. Enable the module and configure admin/config/content/active_campaign be sure
   to complete all required fields.  If the module is improperly configured
   a warning will be displayed on active_campaign pages.